/*

 */
#include <Arduino.h>
byte inS = 2; // Interrupt Pin. While held High a command is sent. byte inB 
= 5; // The command is specified by the number of pulses on this pin while 
inS is held High. byte ledClr[8][3] = 
{{255,0,0},{0,255,0},{0,0,255},{255,0,0},{0,255,0},{0,0,255},{255,0,0},{0,255,0}}; 
// Predefined colors for the RGB LED.
byte ledPins[3] = {9,10,11}; // Pins for the RGB led. byte motorPins[4] = 
{4,7,8,12}; void setup() {
  // initialize I/O Pins.
  pinMode(inS, INPUT); pinMode(inB, INPUT); pinMode(4, OUTPUT); for (byte 
  uA = 7; uA <= 13; uA++){
    pinMode(uA, OUTPUT);
  }
  
}
void loop() {
  // turn the LED on (HIGH is the voltage level)
  digitalWrite(LED_BUILTIN, HIGH);
  // wait for a second
  delay(1000);
  // turn the LED off by making the voltage LOW
  digitalWrite(LED_BUILTIN, LOW);
   // wait for a second
  delay(1000);
}
int cmd() {
  // Read incoming command code
  bool currentState =digitalRead(inB); byte count = 0; while 
  (digitalRead(inS)) {
    if (currentState =! digitalRead(inB)){ delay(20); if (currentState =! 
      digitalRead(inB)){ // Checking twice to make sure no false readings 
      have occured.
        count++;
    }
    else { delay(20);
    }
  }
  }
  return count;
}
void blink(byte ldN, byte nBlinks){ for (byte uA = abs(nBlinks); uA > 0; 
  uA--){
    for (byte uB = 0; uB <=2; uB++){ 
      analogWrite(ledPins[uB],ledClr[ldN][uB]);
    }
    delay(400); for (byte uB = 0; uB <=2; uB++){ 
      analogWrite(ledPins[uB],0);
    }
    delay(500);
  }
}



/* DigitalReadSerial Reads a digital input on pin 2, prints the result to 
  the Serial Monitor This example code is in the public domain. 
  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/ bool sequence[4][4] = {{false, true, false, false}, {true, true, false, 
true}, {true, false, true, true}, {false, false, true, false}}; int state = 
0; bool coils[4] = {false, false, false, false}; int j7 = 4; byte duty = 
80; int mPins[4] = {4,6,7,5};
// int moar[73] = 
// {19,19,19,19,19,19,19,19,19,18,18,18,18,17,17,17,16,16,16,15,15,15,14,14,14,13,13,12,12,12,11,11,10,10,10,9,9,8,8,8,7,7,7,6,6,6,5,5,5,4,4,4,4,3,3,3,3,3,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1};
int moar[73] = 
{19,18,17,17,16,15,15,14,13,12,12,11,11,11,10,10,9,9,9,8,8,8,7,7,7,6,5,5,4,4,3,3,3,3,2,2,2,2,2,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; 
void setup() {
  // initialize serial communication at 9600 bits per second:
    for (byte uA = 0; uA < 4; uA++) { pinMode(mPins[uA],OUTPUT);
  }
  Serial.begin(9600);
}
// the setup routine runs once when you press reset:
void loop() { while (state < 800){ pulse();
}
digitalWrite(mPins[0],HIGH); digitalWrite(mPins[1],LOW); 
digitalWrite(mPins[2],HIGH); digitalWrite(mPins[3],LOW); delay(500); 
digitalWrite(mPins[0],LOW); digitalWrite(mPins[1],LOW); 
digitalWrite(mPins[2],LOW); digitalWrite(mPins[3],LOW); delay(2000); state 
= 0;
}
/* void nxtStp() { for (byte uA = 0; uA < 4; uA++) { coils[uA] = 
    sequence[state % 4][uA];
  }
  state++;
}
*/ void pulse() { for (byte uA = 0; uA < 4; uA++){ if (sequence[state % 
    4][uA]) {
      digitalWrite(mPins[uA],HIGH);
      //Serial.print('X'); Serial.print(mPins[uA]); Serial.print('\t');
    }
    else { digitalWrite(mPins[uA],LOW);
      //Serial.print('O'); Serial.print(mPins[uA]); Serial.print('\t');
    }
  }
  Serial.print('\n'); delay(j7+0.7*moar[min(state,72)]); state++;
}
