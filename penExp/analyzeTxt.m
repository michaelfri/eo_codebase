function sorted = analyzeTxt()
[fN1,fP1] = uigetfile({'*.txt';'*.csv'},'Select instruction file');
[fN2,fP2] = uigetfile('*.mat','Select data file');
cmd = procInst([fN1,fP1]);
load([fN2 fP2],'P1A');
clearvars fN1 fP1 fN2 fP2;
for uA = 1:length(cmd)
    sorted{uA} = mrgL(P1A(1,cmd{uA,1}))
    sorted{uA}.Id = cmd{uA,2}(2)
    sorted{uA}.type = cmd{uA,2}(3);
    sorted{uA}.wL = cmd{uA,2}(4:5);
    switch cmd{uA,2}(1)
        case 3
            sorted{uA}.flags = 1;
        case 4
            sorted{uA}.flags = 0;
        case 6
            sorted{uA}.flags = 2;
        otherwise
            sorted{uA}.flags = -1;
    end
    order(uA,:) = [uA sorted{uA}.Id];
end
order = sortrows(order,2);
sorted = sorted(order(:,1));
end
    