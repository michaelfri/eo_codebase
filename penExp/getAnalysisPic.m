function [ltr,all] = getAnalysisPic(varargin)

trs = 0.02;
blob = fspecial('disk',12);


if nargin == 0
    [fN{2},fN{1}] = uigetfile('*.csv;*.tsv;*.txt');
    fName = [fN{1} fN{2}];
else
    fName = varargin{1};
end
clearvars fN;
opts = delimitedTextImportOptions("NumVariables", 11);
opts.DataLines = [2 Inf];
opts.Delimiter = ",";

opts.VariableNames = ["x", "y", "pressure", "time", "Var5", "Var6", "Var7", "Var8", "orAzimuth", "orAltitude", "Var11"];
opts.SelectedVariableNames = ["time","x", "y", "pressure", "orAzimuth", "orAltitude"];
opts.VariableTypes = ["double", "double", "double", "double", "string", "string", "string", "string", "double", "double", "string"];
opts = setvaropts(opts, [5, 6, 7, 8, 11], "WhitespaceRule", "preserve");
opts = setvaropts(opts, [5, 6, 7, 8, 11], "EmptyFieldRule", "auto");
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";


raw = readtable(fName, opts);

raw = table2array(raw);

raw(:,2) = raw(:,2) - min(raw(:,2));
raw(:,3) = max(raw(:,3)) - raw(:,3);
raw(:,1) = raw(:,1) - min(raw(:,1));

timeData = raw(2:end,1)*[0.5 1] + raw(1:end-1,1)*[0.5 -1];
timeData(timeData(:,2) < trs,:) = [];
temporal = [timeData(:,1)-0.5*timeData(:,2),timeData(:,1)+0.5*timeData(:,2)];
all = [0 0 0 0 0 0];
for uA = 1:(length(temporal) - 1)
    ltr{uA}.data = raw((raw(:,1) >= temporal(uA,2)) & (raw(:,1) <= temporal(uA+1,1)),:);
    ltr{uA}.Id = uA;
    ltr{uA}.type = -1;
    ltr{uA}.flags = -1;
    ltr{uA}.position = [mean(ltr{uA}.data(:,2)) mean(ltr{uA}.data(:,3))];
    ltr{uA}.time = min(ltr{uA}.data(:,1));
    all = [all; ltr{uA}.data];
    ltr{uA}.data = [ltr{uA}.data(:,1) - ltr{uA}.time,ltr{uA}.data(:,2) - ltr{uA}.position(1),ltr{uA}.data(:,3) - ltr{uA}.position(2), ltr{uA}.data(:,4:6)];
end
all(1,:) = [];


%  [dV,dY,dX] = histcounts2(all(:,2),all(:,1),[186*round((max(all(:,1))-min(all(:,1)))/(max(all(:,2))-min(all(:,2)))),186]);
% dY = 0.5*(dY(2:end)+dY(1:end-1));
% dX = 0.5*(dX(2:end)+dX(1:end-1));
% 
% uX = [1 1];
% for uA = 1:length(dX)
%     for uB = 1:length(dY)
%         if dV(uA,uB) ~= 0
%             dP(uX(1),1:3) = [dX(uA) dY(uB) dV(uA,uB)];
%             uX(1) = uX(1) + 1;
%         else
%             dQ(uX(2),1:3) = [dX(uA) dY(uB) dV(uA,uB)];
%             uX(2) = uX(2) + 1;
%         end
%     end
% end

% dims = size(dV);

% [grid(1),grid(2)] = rat(dims(1)/dims(2),0.005);
% if min(grid) < 7
%     grid = grid * round(7/min(grid));
% end
% for uA = 1:dims(1)
%     for uB = 1:dims(2)
%         dW(uA,uB) = sum(sum(dV((max(1,uA-11):min(dims(1),uA+11)),(max(1,uB-11):min(dims(2),uB+11)))));
%     end
% end
% dW = imfilter(dV,blob);
% interval = mean(dW(dW ~= 0))*[0.1 0.4];
% dW(dW < interval(1) | dW > interval(2)) = 0;
% dW = imfilter(dW,blob);
% uX = 1;
% for uA = 1:length(dY)
%     for uB = 1:length(dX)
%         map(uX,1:5) = [dY(uA) dX(uB) dW(uA,uB) mod(dY(uA),grid(1)) mod(dX(uB),grid(2))];
%         uX = uX + 1;
%     end
% end
% interval = mean(map(map(:,3) ~= 0))*[0.1 0.4];
% map(map(:,3) < interval(1) | map(:,3) > interval(2),:) = [];
% clearvars uX interval;

image = figure('Name','Output Text');
aX = axes('Parent',image,'Position',[0 0 1 1],'DataAspectRatio',[1 1 1],'XLim',[min(raw(:,3)) max(raw(:,3))],'YLim',[min(raw(:,2)) max(raw(:,2))]);
for uA = 1:length(ltr)
    G{uA} = plot(aX,ltr{uA}.data(:,2)+ltr{uA}.position(1),ltr{uA}.data(:,3)+ltr{uA}.position(2),'LineWidth',2);
    C(uA,1:3) = G{uA}.Color;
    G{uA}.Color = [0.4 0.4 0.4];
    hold on
end
daspect([1 1 1]);
for uA = 0:(length(ltr)-1)
    pause(0.1);
    if uA >= 1
    P1 = ginput(1);
    %P1 = [randi([min(raw(:,2)) max(raw(:,2))]) randi([min(raw(:,3)) max(raw(:,3))])];
    T{uA} = text(P1(1),P1(2),num2str(uA));
    %P2 = ginput(1);
    P2 = [ltr{uA}.data(1,2)+ltr{uA}.position(1),ltr{uA}.data(1,3)+ltr{uA}.position(2)];
    L{uA} = line([P1(1) P2(1)],[P1(2) P2(2)],'Color',C(uA,:));
    clear G{uA};
    S{uA} = plot(aX,ltr{uA}.data(:,2)+ltr{uA}.position(1),ltr{uA}.data(:,3)+ltr{uA}.position(2),'LineWidth',2,'Color',C(uA,:));
    end
    hold on
    G{uA+1} = plot(aX,ltr{uA+1}.data(:,2)+ltr{uA+1}.position(1),ltr{uA+1}.data(:,3)+ltr{uA+1}.position(2),'LineWidth',2,'Color','r');
end
end
