function [stats,image] = ltrShow(ltrs)
    colors = [0,0.447,0.741;0.85,0.325,0.098;0.929,0.694,0.125;0.494,0.184,0.556;0.466,0.674,0.188;0.301,0.745,0.933;0.635,0.078,0.184];
image = figure('Name','Output Text');
aX = axes('Parent',image,'Position',[0 0 1 1],'DataAspectRatio',[1 1 1]);
stats.ltrSpc = 0;
stats.wrdSpc = 0;
stats.meanF = 0;
stats.lSize = 0;
stats.lDims = [0 0];
for uA = 1:(length(ltrs)-1)
    line{uA} = minD(ltrs{uA},ltrs{uA+1});
    switch line{uA}.type
        case 0
            plot(aX,[line{uA}.pt1(1) line{uA}.pt2(1)],[line{uA}.pt1(2) line{uA}.pt2(2)],'bo-');
            stats.wrdSpc = [stats.wrdSpc line{uA}.distance];
        case 1
            plot(aX,[line{uA}.pt1(1) line{uA}.pt2(1)],[line{uA}.pt1(2) line{uA}.pt2(2)],'ro-');
            stats.ltrSpc = [stats.ltrSpc line{uA}.distance];
    end
    hold on
end

for uA = 1:length(ltrs)
    for uB = 1:length(ltrs{uA}.data)
        plot(aX,ltrs{uA}.data{uB}(:,2)+ltrs{uA}.position(1),ltrs{uA}.data{uB}(:,3)+ltrs{uA}.position(2),'LineWidth',2,'Color',colors(1+mod(ltrs{uA}.wL(1),6),:));
        plot(aX,ltrs{uA}.polygon,'LineStyle','--','EdgeColor','b','FaceColor','c','FaceAlpha',0.1500);
        stats.meanF = [stats.meanF nanmean(ltrs{uA}.data{uB}(:,4))];
        %plot(aX,ltrs{uA}.polygon);
        hold on
    end
    stats.lSize = [stats.lSize polyarea(ltrs{uA}.polygon.Vertices(:,1),ltrs{uA}.polygon.Vertices(:,2))];
    stats.lDims = [stats.lDims; max(ltrs{uA}.polygon.Vertices(:,1))-min(ltrs{uA}.polygon.Vertices(:,1)),max(ltrs{uA}.polygon.Vertices(:,2))-min(ltrs{uA}.polygon.Vertices(:,2))]; 
end
stats.ltrSpc(1) = [];
stats.wrdSpc(1) = [];
stats.meanF(1) = [];
stats.lSize(1) = [];
stats.lDims(1,:) = [];
aX.DataAspectRatio = [1 1 1];
disp("Mean Letter Spacing: " + num2str(nanmean(stats.ltrSpc)));
disp("Mean Word Spacing: " + num2str(nanmean(stats.wrdSpc)));
disp("Mean Force per letter: " + num2str(nanmean(stats.meanF)));
disp("Mean Letter Size: " + num2str(nanmean(stats.lSize)));
disp("Mean Letter Dimensions: " + num2str(nanmean(stats.lDims(:,1))) + " X " + num2str(nanmean(stats.lDims(:,2))));
end
