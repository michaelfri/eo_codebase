function lD = minD(ltr1,ltr2)
%
% This function computes the minimum euclidean distance between two
% polygons P1 & P2.
% 
% This function takes two arguments, a third one is optional.
% min_d = min_dist_between_two_polygons(P1,P2,Display_solution);
%   P1 & P2 contain the geometry of polygons.
%   P1 & P2 are structures containing two fields: x & y
%   For example:
% 	P1.x = rand(1,5)+2;
% 	P1.y = rand(1,5);
% 	P2.x = rand(1,3);
% 	P2.y = rand(1,3);
%   Display_solution is a binary variable that enables or not the 
%   plot of the solution.
%
% The function starts by checking if polygons are intersecting. 
% In this case, the minimum distance is 0.
% Otherwise, distances between all vertices and edges of the two
% polygons are computed. The function returns the minimum distance found.
% Further details of the implementation can be found in the code.
% 
% 2008 12 15
% Guillaume Jacquenot
% guillaume dot jacquenot at gmail dot com
%

if nargin > 3
    error('min_dist_between_two_polygons:e0',...
         'Function Compute_closest_distance needs at least two arguments');
end
if ~(isstruct(ltr1) && isstruct(ltr2))
    error('min_dist_between_two_polygons:e1',...
         'P1 & P2 should be structures, containing two fields x & y');
end
nP1 = numel(ltr1.polygon.Vertices(:,1));
nP2 = numel(ltr2.polygon.Vertices(:,1));
if nP1==0 || nP2==0
    error('min_dist_between_two_polygons:e2',...
         'P1 & P2 should be non-empty structures');    
elseif nP1==1 && nP2==1
    lD.distance = sqrt((ltr1.polygon.Vertices(:,1)-ltr2.polygon.Vertices(:,1))^2+(ltr1.polygon.Vertices(:,2)-ltr2.polygon.Vertices(:,2))^2);
    return;
end
if nP1~=numel(ltr1.polygon.Vertices(:,2))
    error('min_dist_between_two_polygons:e3',...
         'Numbers of points of P1.x and P1.y must be equal ');
end
if nP2~=numel(ltr2.polygon.Vertices(:,2))
    error('min_dist_between_two_polygons:e4',...
         'Numbers of points of P2.x and P2.y must be equal ');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Checking if polygons are closed
if ~(ltr1.polygon.Vertices(1,:) == ltr1.polygon.Vertices(end,:))
    l1V = [ltr1.polygon.Vertices; ltr1.polygon.Vertices(1,:)];
    nP1 = nP1 + 1;
else
    l1V = ltr1.polygon.Vertices;
end
if ~(ltr2.polygon.Vertices(1,:) == ltr2.polygon.Vertices(end,:))
    l2V = [ltr2.polygon.Vertices; ltr2.polygon.Vertices(1,:)];
    nP2 = nP2 + 1;
else
    l2V = ltr2.polygon.Vertices;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check whether or not polygons are intersecting
if nP1>1 && nP2>1
    % Bounding box test
    L1mM = [min(l1V(:,1)) min(l1V(:,2)) max(l1V(:,1)) max(l1V(:,2))];
    L2mM = [min(l2V(:,1)) min(l2V(:,2)) max(l2V(:,1)) max(l2V(:,2))];
    
    if ((L1mM(1) < L2mM(4)) && (L1mM(3) > L2mM(2)) && (L1mM(2) < L2mM(3)) && (L1mM(4) > L2mM(2)))
        % Checking if any of the segments of the 2 polygons are crossing
        % S. H?lz function    
        [x,y] = cInt(l1V(:,1),l1V(:,2),l2V(:,1),l2V(:,2));
        if ~isempty(x)
            lD.distance = 0;
%             if Display_solution
%                 figure
%                 hold on
%                 box on
%                 axis equal
%                 plot(l1V(:,1),l1V(:,2),l2V(:,1),l2V(:,2));
%                 plot(x,y,'ro');   
%             end            
            return;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To compute the minimum distance between two polygons, we compute the
% minimum distance between all vertices and all edges.
% To compute this distance, one needs to compute the minimum distance
% between a vertex I (xi,yi) and an edge [A B] of
% coordinates (xa,ya) and (xb,yb)
% To compute this distance, one computes the projected point of the vertex
% I on the line passing through the vertices of the edge [A B].
% The projected point P corresponds to the minimum distance between the
% vertex and the line.
% The coordiantes of point P are computed in a parametric way with the line
% created by points A and B:
% Each line is represented with a parametric representation
% x = (xb-xa)*k + xa;
% y = (yb-ya)*k + ya;
% To find the parametric coordinate of point P, we search for the minimum
% of the parabola ruling the distance between point P and line (AB).
% d = a*k^2+b*k
% a =   (xa-xb)^2+(ya-yb)^2
% b = 2((xa-xb)(xa-xi)+(ya-yb)(ya-yi));
% The minimum of a parabola occurs for k = -b/(2a);
% Three different cases are possible
%   if k < 0, the point is located before point A.
%   if k > 1, the point is located below point B.
%   if 0 <= k <= 1, the point is located on segment [A B]. 
% The minimal distance dmin between vertex I and segment [A B] depends
% on the value of k
%   if k < 0, dmin = distance(I,A);
%   if k > 1, dmin = distance(I,B);
%   if 0 <= k <= 1, dmin = distance(I,P); 
% To find the minimum distances between 2 polygons, we compute the minimum
% distance between all vertices of P1 and all edges of P2, and vice versa
%
% I have tried to minimise the number of for loop.
%%%%%%%%%%%%%%%%%%%%%%%%
% All points of polygon P2 are projected along every line created from
% every segment of P1
% A loop is used to sweep along segments of P1.
if nP1 >= 3
     dL1 = [diff(l1V(:,1)) diff(l1V(:,2))];
    sdP12 = dL1(:,1).^2 + dL1(:,1).^2;
%     if any(sdP12 == 0)
%         warning('min_dist_between_two_polygons:w1','Two successive points of P1 are indentical.')
%     end
    K1 = zeros(nP1-1,nP2);
    D1 = zeros(nP1-1,nP2);
    for uX = 1:numel(l1V(:,1))-1
        % Compute the closest distance between a point and a segment
        % Computation of the parameter k
        k = -(dL1(uX,1) * (l1V(uX,1) - l2V(:,1)) + dL1(uX,2) * (l1V(uX,2) - l2V(:,2))) / sdP12(uX);
        I1 = k < 0;    
        I2 = k > 1;
        I  =~(I1|I2);
        % Computation of the minimum squared distances between all points of 
        % P2 and the i-th segment of P1.
        D1(uX,I1) = (l2V(I1,1) - l1V(uX,1)).^2+(l2V(I1,2)-l1V(uX,2)).^2;
        D1(uX,I2) = (l2V(I2,1) - l1V(uX+1,1)).^2+(l2V(I2,2)-l1V(uX+1,2)).^2;
        D1(uX,I) = (l2V(I,1) - (l1V(uX,1)+k(I)*dL1(uX,1))).^2 + (l2V(I,2)-(l1V(uX,2)+k(I)*dL1(uX,1))).^2;
        K1(uX,:) = k;
    end
    % One computes the minimum distance for polygon 1
    [mD1,iS1] = min(D1);
    [mmD1,iP1] = min(mD1);
end
%%%%%%%%%%%%%%%%%%%%%%%%
% The same operations are performed for all segments of P2.
if nP2 >= 3
     dL2 = [diff(l2V(:,1)) diff(l2V(:,2))];
    sdP22 = dL2(:,1).^2 + dL2(:,1).^2;
%     if any(sdP22 == 0)
%         warning('min_dist_between_two_polygons:w2','Two successive points of P1 are indentical.')
%     end
    K2 = zeros(nP2-1,nP1);
    D2 = zeros(nP2-1,nP1);
    for uX = 1:numel(l2V(:,1))-1
        % Compute the closest distance between a point and a segment
        % Computation of the parameter k
        k = -(dL2(uX,1) * (l2V(uX,1) - l1V(:,1)) + dL2(uX,2) * (l2V(uX,2) - l1V(:,2))) / sdP22(uX);
        I1 = k < 0;    
        I2 = k > 1;
        I  =~(I1|I2);
        % Computation of the minimum squared distances between all points of 
        % P2 and the i-th segment of P1.
        D2(uX,I1) = (l1V(I1,1) - l2V(uX,1)).^2+(l1V(I1,2)-l2V(uX,2)).^2;
        D2(uX,I2) = (l1V(I2,1) - l2V(uX+1,1)).^2+(l1V(I2,2)-l2V(uX+1,2)).^2;
        D2(uX,I) = (l1V(I,1) - (l2V(uX,1)+k(I)*dL2(uX,1))).^2 + (l1V(I,2)-(l2V(uX,2)+k(I)*dL2(uX,2))).^2;
        K2(uX,:) = k;
    end
    % One computes the minimum distance for polygon 1
    [mD2,iS2] = min(D2);
    [mmD2,iP2 ] = min(mD2);
end

if (nP1 >= 3) && (nP2 >= 3)
    [lD.distance,iT] = min([mmD1,mmD2]);
    lD.distance = sqrt(lD.distance);
elseif nP1 < 3
    iT = 2;
    lD.distance = sqrt(mmD2);    
elseif nP2 < 3
    iT = 1;
    lD.distance = sqrt(mmD1);    
end
    if iT == 1
        k = K1(iS1(iP1),iP1);
        if k > 1
            lD.pt1 = [l1V(iS1(iP1)+1,1) l1V(iS1(iP1)+1,2)];
        elseif k < 0
            lD.pt1 = [l1V(iS1(iP1),1) l1V(iS1(iP1),2)];
        else
            lD.pt1 = [l1V(iS1(iP1),1)+k*dL1(iS1(iP1),1) l1V(iS1(iP1),2)+k*dL1(iS1(iP1),2)];
        end
        lD.pt2 = [l2V(iP1,1) l2V(iP1,2)];
    else
        k = K2(iS2(iP2),iP2);
        if k > 1
            lD.pt2 = [l2V(iS2(iP2)+1,1) l2V(iS2(iP2)+1,2)];
        elseif k < 0
            lD.pt2 = [l2V(iS2(iP2),1) l2V(iS2(iP2),2)];
        else
            lD.pt2 = [l2V(iS2(iP2),1)+k*dL2(iS2(iP2),1) l2V(iS2(iP2),2)+k*dL2(iS2(iP2),2)];
        end
        lD.pt1 = [l1V(iP2,1) l1V(iP2,2)];  
    end
    if ltr1.wL == ltr2.wL
        lD.type = 1;
    elseif ltr1.wL(2) == ltr2.wL(2)
        lD.type = 0;
    else 
        lD.type = -1;
    end
end
