function l = mrgL(ll)
if length(ll) == 1
    l.position = ll{:}.position;
    l.time = ll{:}.time;
    l.Id = ll{:}.Id;
    l.data{1} = ll{:}.data;
    l.type = ll{:}.type;
    l.flags = ll{:}.flags;
    pts = ll{:}.data(boundary(ll{:}.data(:,2),ll{:}.data(:,3),0.5),2:3) + ll{:}.position;
    l.polygon = polyshape(pts(:,1),pts(:,2));
else
    %l.data = [ll{1}.data(:,1),ll{1}.data(:,2) + ll{1}.position(1),ll{1}.data(:,3) + ll{1}.position(2),ll{1}.data(:,4:end)];
    l.time = ll{1}.time;
    nums = [0 ll{1}.position(1) ll{1}.position(2) 0 0 0];
    l.data{1} = ll{1}.data + nums;
    pos = ll{1}.data(:,2:3) + nums(2:3);
    for uA = 2:length(ll)
        nums = [max(ll{uA-1}.data(:,1)) ll{uA}.position(1) ll{uA}.position(2) 0 0 0];
        l.data{uA} = ll{uA}.data + nums;
        pos = [pos; ll{uA}.data(:,2:3) + nums(2:3)];
        l.time = min(l.time,ll{uA}.time);
    end
    l.position = mean(pos);
    pts = pos(boundary(pos(:,1),pos(:,2),0.5),:);
    l.polygon = polyshape(pts(:,1),pts(:,2));
    for uA = 1:length(l.data)
        l.data{uA} = l.data{uA} - [0 l.position 0 0 0];
    end
end