function w = mrgW(wW)
if length(wW) == 1
    w.position = wW{:}.position;
    w.time = wW{:}.time;
    w.Id = wW{:}.wL;
    w.data{1} = wW{:}.data;
else
    %l.data = [ll{1}.data(:,1),ll{1}.data(:,2) + ll{1}.position(1),ll{1}.data(:,3) + ll{1}.position(2),ll{1}.data(:,4:end)];
    w.time = wW{1}.time;
    nums = [0 wW{1}.position(1) wW{1}.position(2) 0 0 0];
    w.data{1} = wW{1}.data + nums;
    pos = wW{1}.data(:,2:3) + nums(2:3);
    for uA = 2:length(wW)
        nums = [max(wW{uA-1}.data(:,1)) wW{uA}.position(1) wW{uA}.position(2) 0 0 0];
        w.data{uA} = wW{uA}.data + nums;
        pos = [pos; wW{uA}.data(:,2:3) + nums(2:3)];
        w.time = min(w.time,wW{uA}.time);
    end
    w.position = mean(pos);
    for uA = 1:length(w.data)
        w.data{uA} = w.data{uA} - [0 w.position 0 0 0];
    end
end