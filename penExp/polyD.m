 function [p1,p2,d] = polyD(L1,L2)
 minD =  [L1.Verts(1,:) L2.Verts(1,:) pdist([L1.Verts(1,:); L2.Verts(1,:)],'euclidean')];
             for uA = 1:length(L1.Verts)
                 for uB = 1:length(L2.Verts)
                     disp([L1.Verts(uA,:) L2.Verts(uB,:) pdist([L1.Verts(uA,:); L2.Verts(uB,:)],'euclidean')]);
                     dists{uA,uB} = [L1.Verts(uA,:) L2.Verts(uB,:) pdist([L1.Verts(uA,:); L2.Verts(uB,:)],'euclidean')];
                     if dists{uA,uB}(5) < minD(5)
                         minD = dists{uA,uB}(5);
                     end
                 end
             end
             p1 = minD(1:2);
             p2 = minD(3:4);
             d = minD(5);
 end
figure('units','normalized','outerposition',[0 0 1 1]);
axes('Units', 'normalized', 'Position', [0 0 1 1]);
for i = 1:28
h(i) = plot(L2{i}.Polygon);
h(i).LineStyle = '--';
h(i).EdgeColor = 'b';
h(i).FaceColor = 'c';
h(i).FaceAlpha =  0.1500;
hold on
top = find(L2{i}.Verts(:,2) <= (min(L2{i}.Verts(:,2))));
txt = "$\frac{\uparrow}{"+ num2str(i) + "}$";
text(L2{i}.Verts(top(1),1),L2{i}.Verts(top(1),2),txt,'Interpreter','latex','VerticalAlignment','top','HorizontalAlignment','center','FontSize',12,'Rotation',0)
plot(L2{i}.VertsG(:,1),L2{i}.VertsG(:,2),'LineWidth',2);
plot(L2{i}.Center(1),L2{i}.Center(2),'r*');
plot(L2{i}.VertsG(:,1),L2{i}.VertsG(:,2)+80,'LineWidth',2,'Color',[0 0 0.2]);
end

figure('units','normalized','outerposition',[0 0 1 1]);
axes('Units', 'normalized', 'Position', [0 0 1 1]);
for i = 1:139
plot(L{i}.VertsG(:,1),L{i}.VertsG(:,2),'LineWidth',2,'Color',[0 0 0.2]);
end