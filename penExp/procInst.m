function cmd = procInst(file)
% import data containing letter array
f_iD = fopen(file);
raw = textscan(f_iD,'%s','Delimiter',',');
fclose(f_iD);
nW = 1;
nL = 1;
for uA = 1:length(raw{1,1})
   uB = 0;
   uC = 0;
   while ~isstrprop(raw{1,1}{uA,1}(uB+1),'alpha')
       uB = uB + 1;
   end
   if uB ~= 0
       a = str2double(raw{1,1}{uA,1}(1:uB));
   else
       a = -1;
   end
   while ~isstrprop(raw{1,1}{uA,1}(end - uC),'alpha')
       uC = uC + 1;
   end
   if uC ~= 0
       c = str2double(raw{1,1}{uA,1}((end - uC + 1):end));
   else
       c = -1;
   end
   b = upper(raw{1,1}{uA,1}((uB+1):(end - uC)));
   uX = 1;
   if uA > 1
   while and((uA - uX) >= 1,command(uA-uX,1) == -1)
       uX = uX + 1;
   end
   d = command(uA-uX,1);
   end
   switch b
       case 'D'
           command(uA,:) = [a,1,c,nL,nW];
       case 'U'
           command(uA,:) = [a,2,c,nL,nW];
       case 'LL'
           command(uA,:) = [a,3,c,nL,nW];
       case 'L'
           command(uA,:) = [a,4,c,nL,nW];
       case 'X'
           command(uA,:) = [a,5,c,nL,nW];
       case 'S'
           command(uA,:) = [a,6,c,nL,nW];
       case 'G'
           nL = nL + 1;
           command(uA,:) = [d,7,c,nL,nW];
       case 'N'
           nL = nL + 1;
           nW = nW + 1;
           command(uA,:) = [d,8,c,nL,nW];
       case 'M'
           command(uA,:) = [a,9,c,nL,nW];
       case 'A'
           command(uA,:) = [a,10,c,nL,nW];
       otherwise
           command(uA,:) = [a,-1,c,nL,nW];
   end
end
           
       U = command(command(:,2) == 2,:);
       cmd{1,1} = U(1,[1,3]);
       for uX = 1:size(U,1)
            for uY = 1:size(cmd,1)
                if ~isempty(intersect(U(uX,[1,3]),cmd{uY,1}))
                    cmd{uY,1} = unique([cmd{uY,1} U(uX,[1,3])]);
                    add = 1;
                    
                end
            end
            if add == 0
                cmd{size(cmd,1)+1,1} = U(uX,[1,3]);
            else
                add = 0;
            end
       end
       U = command(command(:,2) == 3 | command(:,2) == 4 | command(:,2) == 6 | command(:,2) == 10,:);
       sS = size(cmd,1);
       oS = sS;
       for uX = 1:size(U,1)
           for uY = 1:oS
               if ismember(U(uX,1),cmd{uY,1})
                   cmd{uY,2} = [U(uX,2),min(cmd{uY,1}),U(uX,3:5)];
                   add = 1;
               end
           end
           if add == 0
               cmd{sS+1,1} = U(uX,1);
               cmd{sS+1,2} = U(uX,[2,1,3:5]);
               sS = sS + 1;
           else
               add = 0;
           end
       end
end